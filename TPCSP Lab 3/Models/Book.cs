﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPCSP_Lab_3.Models
{
    public class Book
    {
        public int Code { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Year { get; set; }
    }
}
