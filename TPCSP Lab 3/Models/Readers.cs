﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPCSP_Lab_3.Models
{
    public class Readers
    {
        public string LastName { get; set; }
        public int BookCode { get; set; }
        public DateTime DateOfIssue { get; set; }
        public DateTime DateOfReturn { get; set; }
    }
}
