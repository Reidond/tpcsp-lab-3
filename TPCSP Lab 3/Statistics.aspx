﻿<%@ Page Title="Statistics" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Statistics.aspx.cs" Inherits="TPCSP_Lab_3.Statistics" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-12">
            <h3>Statistics</h3>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Book code</th>
                        <th scope="col">Title</th>
                        <th scope="col">Total number of issues</th>
                        <th scope="col">Total number of returns</th>
                    </tr>
                </thead>
                <tbody runat="server" id="statisticsTable">
                </tbody>
            </table>
        </div>
    </div>

</asp:Content>
