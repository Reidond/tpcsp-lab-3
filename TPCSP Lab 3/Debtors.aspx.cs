﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPCSP_Lab_3.VirtualData;

namespace TPCSP_Lab_3
{
    public partial class Debtors : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var virualDataProvider = new VirualDataProvider();

            var debtorsList = virualDataProvider.Readers
                .Where(d => d.DateOfIssue.Year < DateTime.Today.Year && d.DateOfReturn == DateTime.MinValue)
                .ToList();

            foreach (var debtor in debtorsList)
            {
                debtors.InnerHtml += $"<li class='list-group-item list-group-item-danger'>{debtor.LastName}</li>";
            }
        }
    }
}
