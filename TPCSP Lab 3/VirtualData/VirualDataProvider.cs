﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using TPCSP_Lab_3.Models;

namespace TPCSP_Lab_3.VirtualData
{
    public class VirualDataProvider
    {
        private readonly string _path = @"C:\Users\Reidond\source\repos\TPCSP Lab 3\TPCSP Lab 3\VirtualData\";
        public List<Book> Library { get; set; }
        public List<Readers> Readers { get; set; }
        public VirualDataProvider()
        {
            Library = new List<Book>();
            Readers = new List<Readers>();

            ReadData();
        }
        private void ReadData()
        {
            foreach (var line in File.ReadAllLines(_path + "Library.txt", Encoding.GetEncoding(1251)))
            {
                var words = Regex.Split(line, "[^\\d+\\s\"']+|\"([^\"]*)\"|'([^']*)'");
                Library.Add(new Book
                {
                    Code = int.Parse(words[0]),
                    Title = words[1],
                    Author = words[3],
                    Year = words[4].Trim()
                });
            }
            foreach (var line in File.ReadAllLines(_path + "Readers.txt", Encoding.GetEncoding(1251)))
            {
                var words = Regex.Split(line, "\\s[^\\d+\\s\"']+|\"([^\"]*)\"|'([^']*)'");
                Readers.Add(new Readers
                {
                    LastName = words[0].Split(' ')[0],
                    BookCode = int.Parse(words[0].Split(' ')[1]),
                    DateOfIssue = DateTime.Parse(words[1]),
                    DateOfReturn = words[3] == "" ? DateTime.MinValue : DateTime.Parse(words[3])
                });
            }
        }
    }
}
