﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPCSP_Lab_3.Models;
using TPCSP_Lab_3.VirtualData;

namespace TPCSP_Lab_3
{
    public partial class Statistics : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var virtualDataProvider = new VirualDataProvider();

            var datesOfIssues = new Dictionary<int, int>();
            var datesOfReturns = new Dictionary<int, int>();

            foreach (var book in virtualDataProvider.Library)
            {
                datesOfIssues.Add(book.Code, virtualDataProvider.Readers.Count(element => element.DateOfIssue > DateTime.MinValue && element.BookCode == book.Code));

                datesOfReturns.Add(book.Code, virtualDataProvider.Readers.Count(element => element.DateOfReturn != DateTime.MinValue && element.BookCode == book.Code));
            }

            foreach (var book in virtualDataProvider.Library)
            {
                statisticsTable.InnerHtml += $"<tr><th scope='row'>{book.Code}</th><td>{book.Title}</td><td>{datesOfIssues[book.Code]}</td><td>{datesOfReturns[book.Code]}</td></tr>";
            }
        }
    }
}
