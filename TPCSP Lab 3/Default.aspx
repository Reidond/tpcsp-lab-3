﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TPCSP_Lab_3._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-6">
            <h3>Books</h3>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Book code</th>
                        <th scope="col">Title</th>
                        <th scope="col">Author</th>
                        <th scope="col">Year of publish</th>
                    </tr>
                </thead>
                <tbody runat="server" id="libraryTable">
                </tbody>
            </table>
        </div>
        <div class="col-6">
            <h3>Readers</h3>
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Last name</th>
                        <th scope="col">Book code</th>
                        <th scope="col">Date of issue</th>
                        <th scope="col">Date of return</th>
                    </tr>
                </thead>
                <tbody runat="server" id="readersTable">
                </tbody>
            </table>
        </div>
    </div>

</asp:Content>
