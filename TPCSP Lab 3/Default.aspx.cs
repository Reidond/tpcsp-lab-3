﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPCSP_Lab_3.VirtualData;

namespace TPCSP_Lab_3
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var virualDataProvider = new VirualDataProvider();

            foreach(var book in virualDataProvider.Library)
            {
                libraryTable.InnerHtml += $"<tr><th scope='row'>{book.Code}</th><td>{book.Title}</td><td>{book.Author}</td><td>{book.Year}</td></tr>";
            }

            foreach (var reader in virualDataProvider.Readers)
            {
                var dateOfReturn = reader.DateOfReturn == DateTime.MinValue ? "Book not returned" : reader.DateOfReturn.ToString();
                readersTable.InnerHtml += $"<tr><th scope='row'>{reader.LastName}</th><td>{reader.BookCode}</td><td>{reader.DateOfIssue}</td><td>{dateOfReturn}</td></tr>";
            }
        }
    }
}
